package com.Best.of.Best.top.gadget.sqrenchiki

import android.app.Activity
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.Best.of.Best.top.gadget.Knopochka
import com.Best.of.Best.top.gadget.MainBackground
import com.Best.of.Best.top.gadget.R

@Composable
fun MenSqreen(goToProducts:()->Unit) {
    val fh = LocalConfiguration.current.screenHeightDp
    val conec = LocalContext.current as Activity
    Box(modifier = Modifier.fillMaxSize())
    {
        MainBackground()

        Column(modifier = Modifier
            .align(Alignment.Center)
            .padding(bottom = (fh * 0.2).dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy((fh*0.1).dp)) {
            Knopochka(wtd = { goToProducts.invoke() }) {
                Image(painter = painterResource(id = R.drawable.favproducts), contentDescription ="" )
            }
            Knopochka(wtd = { conec.finish() }) {
                Image(painter = painterResource(id = R.drawable.exitbutton), contentDescription ="" )
            }

        }
    }
    BackHandler {

    }
}