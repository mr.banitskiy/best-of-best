package com.Best.of.Best.top.gadget

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.sp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Composable
fun Textik(text:String, size:Int, color:Color, modifier: Modifier) {

    Text(text = text, fontSize = size.sp, color = color, modifier = modifier)
}

@Composable
fun MainBackground()
{
    Image(painter = painterResource(id = R.drawable.mainbg), contentDescription ="",
        modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
    Image(painter = painterResource(id = R.drawable.mainbg2), contentDescription ="",
        modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
}

@Composable
fun Knopochka(wtd:()->Unit, modifier: Modifier = Modifier, cont: @Composable ()->Unit ){
    val scalick = remember {
        mutableStateOf(1.2f)
    }
    val cs = CoroutineScope(Dispatchers.Main)
    Box(modifier = modifier.then(
        Modifier
            .scale(scalick.value)
            .clickable {
                cs.launch {
                    scalick.value = 0.5f
                    delay(150)
                    scalick.value = 0.8f
                    wtd.invoke()
                    scalick.value = 1.2f
                }

            }), contentAlignment = Alignment.Center){
        cont.invoke()
    }
}