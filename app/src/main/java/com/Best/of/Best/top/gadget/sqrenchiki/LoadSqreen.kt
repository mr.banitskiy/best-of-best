package com.Best.of.Best.top.gadget.sqrenchiki

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import com.Best.of.Best.top.gadget.R
import kotlinx.coroutines.delay

@Composable
fun LoadSqreen(gNext:()->Unit) {

    LaunchedEffect(Unit)
    {
        delay(2000)
        gNext.invoke()
    }
    Box(modifier = Modifier.fillMaxSize())
    {
        Image(painter = painterResource(id = R.drawable.plashbg), contentDescription ="",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.Crop)
    }

}