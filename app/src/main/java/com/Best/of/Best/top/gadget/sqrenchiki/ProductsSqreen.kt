package com.Best.of.Best.top.gadget.sqrenchiki

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.Best.of.Best.top.gadget.Knopochka
import com.Best.of.Best.top.gadget.MainBackground
import com.Best.of.Best.top.gadget.R
import com.Best.of.Best.top.gadget.Textik
import com.Best.of.Best.top.gadget.ui.theme.White

@Composable
fun ProductsSqreen(gBack:()->Unit) {
    val fh = LocalConfiguration.current.screenHeightDp
    val fw = LocalConfiguration.current.screenWidthDp

    val products = remember{
        mutableListOf(
            R.drawable.img1,
            R.drawable.img2,
            R.drawable.img3,
            R.drawable.img4,
            R.drawable.img5,
            R.drawable.img6,
            R.drawable.img7,
            R.drawable.img8,
            R.drawable.img9,
            R.drawable.img10,
            R.drawable.img11,
            R.drawable.img12,
            R.drawable.img13,
            R.drawable.img14,
            R.drawable.img15,
            R.drawable.img16,
            R.drawable.img17,
            R.drawable.img18,
        )
    }
    val productsDescription = remember{
        mutableListOf(
            "Foldable Android smartphones became even more appealing upgrade options for consumers in 2023, and the Samsung Galaxy Z Flip 5 is our top pick in this growing product category.",
            "The Soundcore Motion X600 Bluetooth speaker is Anker’s best yet and our favorite of the year, handily besting bigger-name rivals in audio performance and onboard features.",
            "Among the countless laptop accessories we evaluated over the past year, the Anker Prime 20K PowerBank stood out with its ability to fully replace a fast wall charger on the go. Its onboard USB-C ports can simultaneously power up two laptops with 100 watts of output — an incredible feat for a battery pack. Believe it or not",
            "The Soundcore Liberty 4 NC shattered our expectations of what a sub-$100 set of true wireless earbuds is capable of in 2023 by delivering strong noise cancellation, excellent sound that’s infinitely customizable, and long battery life in a sleek and durable form factor that’s available in several colors.",
            "Sony's all-new WF-1000XM5 true wireless earbuds are the best in their class and a worthy follow-up to a legendary product. Compared to the previous iteration, the new earbuds produce superior sound and noise cancellation courtesy of upgraded drivers and audio processing chips.",
            "Many low-cost laptops and Chromebooks skimp on up-to-date hardware and connectivity features, but not the Acer Aspire 3. The Windows 11 laptop became our top pick under $500 by sporting a sharp display, a rich set of wired and wireless connectivity tools, and a lightweight build.",
            "The TCL QM8 is our pick for the best big-screen TV of 2023 because it over-delivers in design, performance, and picture quality for a product in its price category (it costs much less than a comparable OLED TV). Its Quantum dot display panel with mini LED backlighting and thousands of local dimming zones produces incredible maximum brightness without skimping on contrast and color accuracy.",
            "Like its stellar predecessor, the Google Pixel 7a is the best phone for the money — we’d take it over any similarly priced rival. The sub-$500 Android impressed us with its elegant design, futureproof hardware, excellent everyday performance, superb camera, and solid battery life with onboard wireless charging. In addition, the device runs a clutter-free",
            "The latest Amazon Echo Show 5 smart display is our favorite Alexa-powered gadget to hit the shelves this year. For less than $100, the connected home accessory offers an elegant design, a capable speaker for blasting tunes, and a 5.5-inch touchscreen. In addition to being handy for making video calls with other Amazon Echo users",
            "The Nanoleaf Essentials A19 is the best smart lightbulb of 2023. The super affordable (it costs less than $20) gadget impressed us with easy installation that doesn’t require additional hubs and integrated support for Amazon Alexa, Google Assistant, and Siri, guaranteeing you can control it with your go-to virtual assistant.",
            "By thoroughly refreshing an iconic product, Beats by Dre brought us the best wireless headphones of the year. The Beats Studio Pro over-ear headphones offer top-tier sound with support for immersive spatial audio, excellent noise cancellation, and long battery life in an instantly recognizable package with superb ergonomics.",
            "This multi-port USB-C wall charger by Nomad — an accessory maker with a solid track record of making great products — is incredibly sleek, compact, and capable of rapidly powering up phones, tablets, MacBooks, and countless other laptops and small gadgets. The futureproof accessory is the only travel charger a gearhead will need and a great buy. — S.V.",
            "Samsung’s Galaxy Tab S9 is one of our favorite tech products of the year because it’s the first Android tablet that can rival the iPad Pro in years. The device’s strikingly thin design, IP68 waterproof build that sets it apart from all rivals, and top-tier hardware specs that include a captivating AMOLED display in three size options and bundled S Pen stylus make it a formidable iPad alternative for Android users. — S.V.",
            "By treating the excellent MacBook Air to a bigger screen and more capable speakers, Apple delivered our favorite laptop for most consumers in 2023. Equipped with the Apple M2 chip, the 15-inch MacBook Air is miles ahead of any similarly priced big-screen rivals regarding real-life performance and battery life. The notebook's elegant design, lightweight build, and agreeable starting price make it a great buy. — S.V.",
            "As its name suggests, the all-new Razer Kishi v2 Pro is an upgraded take on one of the best smartphone gaming controllers. Adding haptic vibrations and a standard audio jack to complement the buttons and triggers with best-in-class tactility made the Kishi v2 Pro our favorite smartphone gaming controller of 2023. ",
            "Although the Apple iPhone 14 Pro Silicone Case with MagSafe I was gifted was comfortable to hold, it didn’t fit my storage needs — that’s when I found this Spigen case on Amazon. The exterior card slot has enough space to hold up to three cards comfortably, sparing me from bringing my wallet everywhere.",
            "There's an abundance of vacuums on the market, but they can't compete with Dyson's latest innovation. The princely price tag is worth it for several reasons: I grew up in a Dyson household, so I've experienced their products' long-term durability and quality, and this smart vacuum is no exception.",
            "We reach for this Dirt Devil Grab and Go at least 3 times a day, if not more. It sits charging in the corner of the kitchen countertop, waiting to spring into action after every meal when the floor and high chair are inevitably covered in a sea of crumbs. It only weighs 1.12 pounds but has strong suction and can clean out all the deep, dark"

        )
    }
    val productsName = remember{
        mutableListOf(
            "Samsung Galaxy Z Flip 5\nAndroid Smartphone",
            "Soundcore Motion X600\nWireless Speaker",
            "Anker Prime 20K PowerBank",
            "Soundcore Liberty 4 NC",
            "Sony WF-1000XM5 Wireless\nEarbuds",
            "Acer Aspire 3 PC Laptop",
            "TCL QM8 Smart 4K TV",
            "Google Pixel 7a Android\n Smartphone",
            "Amazon Echo Show 5 (3rd\nGen)",
            "Nanoleaf Essentials A19 Smart\nBulb",
            "Beats Studio Pro Wireless\nHeadphones",
            "Nomad 130W Power Adapter",
            "Samsung Galaxy Tab S9\nAndroid Tablet",
            "Apple MacBook Air (15-inch)",
            "Razer Kishi V2 Pro",
            "Spigen Slim Armor CS\nDesigned for iPhone 14 Pro\nCase",
            "Dyson V15 Detect",
            "Dirt Devil Grab and Go"

        )
    }
    val productNum = remember{
        mutableStateOf(0)
    }
    Box(modifier = Modifier.fillMaxSize() )
    {
        MainBackground()
        Knopochka(wtd = {
            if(productNum.value>0)
            {
                productNum.value--
            }
            if(productNum.value==0)
            {
                gBack.invoke()
            } }, modifier = Modifier
            .align(Alignment.TopStart)
            .padding((fh * 0.02).dp)
        ) {
            Image(painter = painterResource(id = R.drawable.backich), contentDescription = "",)
        }

        Textik(text = productsName[productNum.value], size = 20, color = White,
            modifier = Modifier
                .align(Alignment.TopCenter)
                .padding(top = (fh * 0.07).dp))
        Column(modifier = Modifier
            .align(Alignment.Center)
            .size(width = (fw * 0.9).dp, height = (fh * 0.65).dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy((fh*0.05).dp)) {
            Image(painter = painterResource(id = products[productNum.value]), contentDescription ="",)
            Column(modifier = Modifier.verticalScroll(rememberScrollState())) {
                Textik(text = productsDescription[productNum.value], size =20, color = White , modifier =Modifier )
            }
        }
        Image(painter = painterResource(id = R.drawable.nxt), contentDescription ="",
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(bottom = (fh * 0.1).dp)
                .clickable(MutableInteractionSource(),null) {
                    if(productNum.value<17)
                    {
                        productNum.value++
                    }
                    else{
                        productNum.value =0
                    }
                })
    }
}