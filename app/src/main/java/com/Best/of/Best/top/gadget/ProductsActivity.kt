package com.Best.of.Best.top.gadget

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.Best.of.Best.top.gadget.sqrenchiki.LoadSqreen
import com.Best.of.Best.top.gadget.sqrenchiki.MenSqreen
import com.Best.of.Best.top.gadget.sqrenchiki.ProductsSqreen
import com.Best.of.Best.top.gadget.ui.theme.BestOfBestTheme

const val LOADING = "loading"
const val MENU = "menu"
const val PRODUCTS = "products"
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation= ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        val parashka = WindowCompat.getInsetsController(window, window.decorView)
        parashka.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        parashka.hide(WindowInsetsCompat.Type.statusBars())
        WindowCompat.setDecorFitsSystemWindows(window,false)
        setContent {
            BestOfBestTheme {
                val control = rememberNavController()

                NavHost(navController = control, startDestination =LOADING  )
                {
                    composable(LOADING)
                    {
                        LoadSqreen(gNext = {control.navigate(MENU)})
                    }
                    composable(MENU)
                    {
                        MenSqreen(goToProducts = {control.navigate(PRODUCTS)})
                    }
                    composable(PRODUCTS)
                    {
                        ProductsSqreen(gBack = {control.popBackStack()})
                    }
                }
            }
        }
    }
}
